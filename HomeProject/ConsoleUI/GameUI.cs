﻿using System;
using System.ComponentModel;
using GameEngine;

namespace ConsoleUI
{
    public static class GameUI
    {
        private static readonly string _verticalSeparator = "|";
        private static readonly string _horizontalSeparator = "-";
        private static readonly string _centerSeparator = "+";
        
        public static void PrintBoard(Game game)
        {
            var board = game.GetBoard();
            var line = "";
            
            Console.Write("   ");
            for (int topX = 0; topX < game.BoardWidth; topX++)
            {
                line = line + _horizontalSeparator + _horizontalSeparator + _horizontalSeparator;
                if (topX < game.BoardWidth)
                {
                    line = line + _centerSeparator;
                }
                if (topX + 1 >= 10)
                {
                    Console.Write(_verticalSeparator + " " + (topX+1));  
                }
                else
                {
                    Console.Write(_verticalSeparator + " " + (topX+1) + " ");
                }
            }
            Console.Write(_verticalSeparator + "\n" + "   " + _centerSeparator + line);
            Console.Write("\n");
            
            
            for (int y = 0; y < game.BoardHeight; y++)
            {
                if (y + 1 >= 10)
                {
                    Console.Write(y + 1 + " " + _verticalSeparator);
                }
                else
                {
                    Console.Write(y + 1 + "  " + _verticalSeparator);
                }
                line = "";
                for (int x = 0; x < game.BoardWidth; x++)
                {
                    
                    Console.Write(" " + GetSquareState(board[y, x]) + " ");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.Write(_verticalSeparator);
                }
                
                Console.Write("\n");

                line = "";
                if (y < game.BoardHeight - 1)
                {
                    for (int x = 0; x < game.BoardWidth; x++)
                    {
                        line = line + _horizontalSeparator + _horizontalSeparator + _horizontalSeparator;
                        if (x < game.BoardWidth)
                        {
                            line = line + _centerSeparator;
                        }
                    }
                    Console.WriteLine("   " + _centerSeparator + line);
                }
            }
        }
        
        public static string GetSquareState(SquareState squareState)
        {
            switch (squareState)
            {
                case SquareState.NotSelected:
                    return " ";
                case SquareState.Empty:
                    return "~";
                case SquareState.Mine:
                    Console.ForegroundColor = ConsoleColor.Red;
                    return "X";
                case SquareState.Flag:
                    Console.ForegroundColor = ConsoleColor.Red;
                    return "F";
                case SquareState.Bomb1:
                    Console.ForegroundColor = ConsoleColor.Blue;
                    return "1";
                case SquareState.Bomb2:
                    Console.ForegroundColor = ConsoleColor.Green;
                    return "2";
                case SquareState.Bomb3:
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    return "3";
                case SquareState.Bomb4:
                    Console.ForegroundColor = ConsoleColor.Magenta;
                    return "4";
                case SquareState.Bomb5:
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    return "5";
                case SquareState.Bomb6:
                    Console.ForegroundColor = ConsoleColor.DarkCyan;
                    return "6";
                case SquareState.Bomb7:
                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                    return "7";
                case SquareState.Bomb8:
                    Console.ForegroundColor = ConsoleColor.Gray;
                    return "8";
                default:
                    throw new InvalidEnumArgumentException("Unknown enum option!");
            }
        }
    }
}