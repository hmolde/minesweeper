﻿using System;
using System.Collections.Generic;

namespace MenuSystem
{
    public class Menu
    {
        private int _menuLevel { get; set; }
        private const string MenuCommandToExit = "X";
        private const string MenuCommandToPrevious = "P";
        private const string MenuCommandToMainMenu = "M";

        public string Title { get; set; } = default!;
        
        private Dictionary<string, MenuItem> _menuItemsDictionary = new Dictionary<string, MenuItem>();

        public Dictionary<string, MenuItem> MenuItemsDictionary
        {
            get => _menuItemsDictionary;
            set
            {
                _menuItemsDictionary = value;
                if (_menuLevel >= 2)
                {
                    _menuItemsDictionary.Add(MenuCommandToMainMenu,
                        new MenuItem(){Title = "Main menu"});
                }
                if (_menuLevel >= 1)
                {
                    _menuItemsDictionary.Add(MenuCommandToPrevious,
                        new MenuItem(){Title = "Previous menu"});
                }
                _menuItemsDictionary.Add(MenuCommandToExit,
                    new MenuItem(){Title = "Exit"});
            }
        }

        public Menu(int menuLevel = 0)
        {
            _menuLevel = menuLevel;
        }

        public string Run()
        {
            var userInput = "";
            
            do
            {
                //Console.Clear();
                Console.WriteLine(Title);
                Console.WriteLine("==========");
                
                foreach (var menuItem in MenuItemsDictionary)
                {
                    Console.WriteLine(menuItem.Key + " " + menuItem.Value);
                }
                
                Console.WriteLine("----------");
                Console.Write("Input: -> ");

                userInput = Console.ReadLine()?.Trim().ToUpper() ?? "";
                
                
                //Check commands
                var returnCommand = "";

                if (MenuItemsDictionary.ContainsKey(userInput))
                {
                    var menuItem = MenuItemsDictionary[userInput];
                    if (menuItem.ExecuteMenuItemCommand != null)
                    {
                        returnCommand = menuItem.ExecuteMenuItemCommand();
                    }

                    //TODO: REMOVE OR THINK OF MAKING BETTER?
                    if (menuItem.ExecuteMenuItemCommandWithValue != null)
                    {
                        returnCommand = menuItem.ExecuteMenuItemCommandWithValue;
                    }
                }
                
                
                if (returnCommand == MenuCommandToExit)
                {
                    userInput = MenuCommandToExit;
                }
                if (returnCommand == MenuCommandToMainMenu)
                {
                    if (_menuLevel != 0)
                    {
                        userInput = MenuCommandToMainMenu;
                    }
                }


            } while (userInput != MenuCommandToExit &&
                     userInput != MenuCommandToPrevious &&
                     userInput != MenuCommandToMainMenu);

            return userInput;
        }
    }
}