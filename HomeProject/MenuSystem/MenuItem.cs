using System;

namespace MenuSystem
{
    public class MenuItem
    {
        private string _title = default!;

        public string Title
        {
            get => _title;
            set => _title = Validate(value, 1, 32, true);
        }

        public Func<string>? ExecuteMenuItemCommand { get; set; }
        public string? ExecuteMenuItemCommandWithValue { get; set; }

        private static string Validate(string item, int minLength, int maxLength, bool toUpper)
        {
            item = item.Trim();
            if (toUpper)
            {
                item = item.ToUpper();
            }

            if (item.Length < minLength || item.Length > maxLength)
            {
                throw new ArgumentException(
                    $"String is not the correct length ({minLength}-{maxLength})! " +
                    $"Got {item.Length} instead.");
            }

            return item;
        }

        public override string ToString()
        {
            return Title;
        }
    }
}