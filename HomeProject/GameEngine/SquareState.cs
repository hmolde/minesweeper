namespace GameEngine
{
    public enum SquareState
    {
        //States every square can have
        NotSelected,
        Flag,
        Mine,
        Empty,
        Bomb1,
        Bomb2,
        Bomb3,
        Bomb4,
        Bomb5,
        Bomb6,
        Bomb7,
        Bomb8
    }
}