namespace GameEngine
{
    public class GameSettings
    {
        //Classic sizes are 8x8 10mines ; 16x16 40 mines ; 16x30 99mines
        public string GameName { get; set; } = "Minesweeper";
        public int BoardHeight { get; set; } = 8;
        public int BoardWidth { get; set; } = 8;
        public int MineCount { get; set; } = 10;
    }
}