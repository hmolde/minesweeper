﻿using System;
using System.Collections.Generic;

namespace GameEngine
{
    /// <summary>
    /// Minesweeper game
    /// </summary>
    public class Game
    {
        public SquareState[,] Board { get; private set; }
        public SquareState[,] BoardCopy { get; private set; }
        
        public int BoardHeight { get; }
        public int BoardWidth { get; }
        
        public int MineCount { get; }
        private bool GameOver { get; set; }

        public List<(int, int)> MineList { get; set; } = default!;

        
        // New game
        public Game(GameSettings gameSettings)
        {
            if (gameSettings.BoardHeight < 8 || gameSettings.BoardWidth < 8)
            {
                throw new ArgumentException("Board size has to be at least 8x8!");
            }

            BoardHeight = gameSettings.BoardHeight;
            BoardWidth = gameSettings.BoardWidth;
            MineCount = gameSettings.MineCount;

            Board = new SquareState[BoardHeight, BoardWidth];
            BoardCopy = new SquareState[BoardHeight, BoardWidth];
            
            PlaceMines();
            PlaceNumbers();
        }

        // Loading game
        public Game(GameState gameState)
        {
            Board = gameState.Board;
            BoardCopy = gameState.BoardCopy;
            BoardHeight = gameState.BoardHeight;
            BoardWidth = gameState.BoardWidth;
            MineCount = gameState.MineCount;
            MineList = gameState.MineLocations;
        }
        
        public SquareState[,] GetBoard()
        {
            var result = new SquareState[BoardHeight, BoardWidth];
            Array.Copy(Board, result, Board.Length);

            return result;
        }

        // Initialize
        private Tuple<int, int> CreateMine()
        {
            Random rnd = new Random();

            int yMine = rnd.Next(0, BoardHeight);
            int xMine = rnd.Next(0, BoardWidth);

            return Tuple.Create(yMine, xMine);
        }
        
        public void PlaceMines()
        {
            var mineList = new List<(int, int)>();            
            for (int i = 0; i < MineCount; i++)
            {
                bool placed = false;
                do
                {
                    var mine = CreateMine();
                    var mineY = mine.Item1;
                    var mineX = mine.Item2;

                    if (BoardCopy[mineY, mineX] != SquareState.Mine)
                    {
                        BoardCopy[mineY, mineX] = SquareState.Mine;
                        placed = true;
                        mineList.Add((mineY, mineX));
                    }
                    
                } while (placed == false);
            }

            MineList = mineList;
        }
        
        public void PlaceNumbers()
        {
            for (int y = 0; y < BoardHeight; y++)
            {
                for (int x = 0; x < BoardWidth; x++)
                {
                    var mineCount = 0;
                    if (BoardCopy[y, x] != SquareState.Mine)
                    {
                        for (int yy = -1; yy < 2; yy++)
                        {
                            for (int xx = -1; xx < 2; xx++)
                            {
                                if (CheckSquareExist((y - yy), (x-xx)))
                                {
                                    if (BoardCopy[(y - yy), (x - xx)] == SquareState.Mine)
                                    {
                                        mineCount++;
                                    }
                                }
                            }
                        }
                    }

                    if (mineCount > 0 )
                    {
                        GiveSquareNumber(mineCount, y, x);
                    }
                    if (mineCount == 0 && BoardCopy[y, x] != SquareState.Mine)
                    {
                        BoardCopy[y, x] = SquareState.Empty;
                    }
                }
            }
        }

        private void GiveSquareNumber(int mineCount, int y, int x)
        {
            switch (mineCount)
            {
                case 1:
                    BoardCopy[y, x] = SquareState.Bomb1;
                    break;
                case 2:
                    BoardCopy[y, x] = SquareState.Bomb2;
                    break;
                case 3:
                    BoardCopy[y, x] = SquareState.Bomb3;
                    break;
                case 4:
                    BoardCopy[y, x] = SquareState.Bomb4;
                    break;
                case 5:
                    BoardCopy[y, x] = SquareState.Bomb5;
                    break;
                case 6:
                    BoardCopy[y, x] = SquareState.Bomb6;
                    break;
                case 7:
                    BoardCopy[y, x] = SquareState.Bomb7;
                    break;
                case 8:
                    BoardCopy[y, x] = SquareState.Bomb8;
                    break;
            }
        }

        private bool CheckSquareExist(int y, int x)
        {
            if (x >= 0 && y >= 0)
            {
                if (x < BoardCopy.GetLength(0) && y < BoardCopy.GetLength(1))
                {
                    return true;
                }
            }

            return false;
        }
        
        
        // Game
        public void Move(int yPos, int xPos, bool flag)
        {
            if (flag == true)
            {
                if (Board[yPos, xPos] == SquareState.Flag)
                {
                    Board[yPos, xPos] = SquareState.NotSelected;
                }
                else
                {
                    Board[yPos, xPos] = SquareState.Flag;
                }
            }
            else
            {
                if (Board[yPos, xPos] == SquareState.Flag)
                { 
                    Board[yPos, xPos] = SquareState.NotSelected;
                }
                
                else if (BoardCopy[yPos, xPos] == SquareState.Mine)
                {
                    Board[yPos, xPos] = SquareState.Mine;
                    RevealAllMines();
                    GameOver = true;
                }
                
                else if (BoardCopy[yPos, xPos] != SquareState.Mine)
                {
                    if (BoardCopy[yPos,xPos] == SquareState.Empty)
                    {
                        Board[yPos, xPos] = SquareState.Empty;
                        RevealEmptySurrounding(yPos, xPos);
                    }
                    else
                    {
                        CheckAndOpenSquareNumber(yPos, xPos);
                    }
                }
            }
        }

        public void RevealAllMines()
        {
            foreach (var mine in MineList)
            {
                Board[mine.Item1, mine.Item2] = SquareState.Mine;
            }
        }

        private void RevealEmptySurrounding(int y, int x)
        {
            for (int yy = -1; yy < 2; yy++)
            {
                for (int xx = -1; xx < 2; xx++)
                {
                    if (CheckSquareExist((y - yy), (x-xx)))
                    {
                        if (Board[(y - yy), (x - xx)] == SquareState.NotSelected)
                        {
                            Move((y - yy), (x - xx), false);
                        }
                    }
                }
            }
        }

        private void CheckAndOpenSquareNumber(int y, int x)
        {
            var squareState = BoardCopy[y, x];
            switch (squareState)
            {
                case SquareState.Bomb1:
                    Board[y, x] = SquareState.Bomb1;
                    break;
                case SquareState.Bomb2:
                    Board[y, x] = SquareState.Bomb2;
                    break;
                case SquareState.Bomb3:
                    Board[y, x] = SquareState.Bomb3;
                    break;
                case SquareState.Bomb4:
                    Board[y, x] = SquareState.Bomb4;
                    break;
                case SquareState.Bomb5:
                    Board[y, x] = SquareState.Bomb5;
                    break;
                case SquareState.Bomb6:
                    Board[y, x] = SquareState.Bomb6;
                    break;
                case SquareState.Bomb7:
                    Board[y, x] = SquareState.Bomb7;
                    break;
                case SquareState.Bomb8:
                    Board[y, x] = SquareState.Bomb8;
                    break;
            }
        }

        //Checking
        public bool CheckConditions()
        {
            GameOver = countExplosions();
            
            if (GameOver)
            {
                return true;
            }
            
            int countFlag = CountFlags();
            int countMineFlag = CountMineFlags();

            if (countFlag == MineCount && countMineFlag == MineCount)
            {
                return true;
            }
            
            return false;
        }

        public bool countExplosions()
        {
            foreach (var mine in MineList)
            {
                if (Board[mine.Item1, mine.Item2] == SquareState.Mine)
                {
                    return true;
                }
            }
            return false;
        }

        public int CountMineFlags()
        {
            int countMineFlag = 0;

            foreach (var mine in MineList)
            {
                if (Board[mine.Item1, mine.Item2] == SquareState.Flag)
                {
                    countMineFlag++;
                }
            }

            return countMineFlag;
        }

        public int CountFlags()
        {
            int countFlag = 0;
            
            for (int y = 0; y < BoardHeight; y++)
            {
                for (int x = 0; x < BoardWidth; x++)
                {
                    if (Board[y, x] == SquareState.Flag)
                    {
                        countFlag++;
                    }
                }
            }

            return countFlag;
        }
    }
}