using System.Collections.Generic;

namespace GameEngine
{
    public class GameState
    {
        // Playing board
        public SquareState[,] Board { get; set; } = default!;
        // Check board
        public SquareState[,] BoardCopy { get; set; } = default!;
        
        //Hold game settings here too?
        public int BoardHeight { get; set; } = 8;
        public int BoardWidth { get; set; } = 8;
        public int MineCount { get; set; } = 10;

        public List<(int, int)> MineLocations { get; set; } = default!;
    }
}