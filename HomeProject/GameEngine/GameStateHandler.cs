using System;
using Newtonsoft.Json;

namespace GameEngine
{
    public static class GameStateHandler
    {
        private const string FileName = "save1.json";

        public static void SaveGame(GameState gameState, string fileName = FileName)
        {
            using (var writer = System.IO.File.CreateText(fileName))
            {
                var jsonString = JsonConvert.SerializeObject(gameState);
                writer.Write(jsonString);
            }
        }

        public static GameState LoadGame(string fileName = FileName)
        {
            if (System.IO.File.Exists(fileName))
            {
                var jsonString = System.IO.File.ReadAllText(fileName);
                //var res = JsonSerializer.Deserialize<GameState>(jsonString);
                var res = JsonConvert.DeserializeObject<GameState>(jsonString);

                return res;
            }
            
            return new GameState();
        }

        public static string Serialize(GameState gameFile)
        {
            var gameString = JsonConvert.SerializeObject(gameFile);

            return gameString;
        }

        public static GameState Deserialize(string gameFile)
        {
            var res = JsonConvert.DeserializeObject<GameState>(gameFile);
            
            return res;
        }
    }
}