﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConsoleUI;
using DAL;
using Domain;
using GameEngine;
using MenuSystem;
using Microsoft.EntityFrameworkCore;

namespace ConsoleApp
{
    class Program
    {
        private static GameSettings _gameSettings = default!;
        private static GameState _gameState = default!;
        
        private static DbContextOptions<AppDbContext> dbOption = new DbContextOptionsBuilder<AppDbContext>()
            .UseSqlite("Data Source=E:/Kool/5.Semester/C#/Projekt/HomeProject/WebApp/minesweeper.db").Options;
        private static AppDbContext ctx = new AppDbContext(dbOption); //TODO: REMOVE


        static void Main(string[] args)
        {
            Console.Clear();
            
            _gameState = GameStateHandler.LoadGame();
            _gameSettings = GameConfigHandler.LoadConfig();
            
            Console.WriteLine($"Hello {_gameSettings.GameName} game!");
            
            var optionsMenu = new Menu(1)
            {
                Title = "Options",
                MenuItemsDictionary = new Dictionary<string, MenuItem>()
                {
                    {
                        "C", new MenuItem()
                        {
                            Title = "Custom options",
                            ExecuteMenuItemCommand = SaveSettings
                        }
                    }
                }
            };
            
            var saveMenu = new Menu(1)
            {
                Title = "Start a game",
                MenuItemsDictionary = new Dictionary<string, MenuItem>()
                {
                    {
                        "1", new MenuItem()
                        {
                            Title = "Save 1",
                            ExecuteMenuItemCommand = SaveGame1
                        }
                    },
                    {
                        "2", new MenuItem()
                        {
                            Title = "Save 2",
                            ExecuteMenuItemCommand = SaveGame2
                        }
                    },
                    {
                        "3", new MenuItem()
                        {
                            Title = "Save 3",
                            ExecuteMenuItemCommand = SaveGame3
                        }
                    },
                }
            };
            
            var loadMenu = new Menu(1)
            {
                Title = "Load game",
                MenuItemsDictionary = new Dictionary<string, MenuItem>()
                {
                    {
                        "1", new MenuItem()
                        {
                            Title = "Game 1",
                            //ExecuteMenuItemCommandWithValue = LoadGame(1)
                            ExecuteMenuItemCommand = LoadGame1
                        }
                    },
                    {
                        "2", new MenuItem()
                        {
                            Title = "Game 2",
                            ExecuteMenuItemCommand = LoadGame2,
                            //ExecuteMenuItemCommandWithValue = LoadGame(2)
                        }
                    },
                    {
                        "3", new MenuItem()
                        {
                            Title = "Game 3",
                            ExecuteMenuItemCommand = LoadGame3
                        }
                    },
                }
            };
            
            var mainMenu = new Menu()
            {
                Title = $"{_gameSettings.GameName} main menu!",
                MenuItemsDictionary = new Dictionary<string, MenuItem>()
                {
                    {
                        "N", new MenuItem()
                        {
                            Title = "New Game",
                            ExecuteMenuItemCommand = saveMenu.Run
                        }
                    },  
                    {
                        "L", new MenuItem()
                        {
                            Title = "Load Game",
                            ExecuteMenuItemCommand = loadMenu.Run
                        }
                    },
                    {
                        "O", new MenuItem()
                        {
                            Title = "Options",
                            ExecuteMenuItemCommand = optionsMenu.Run
                        }
                    }
                }
            };

            mainMenu.Run();
            
        }
        
        //TODO: Make this compact somehow
        // --------------------------------
        static string LoadGame1()
        {
            LoadGame(1);
            return "";
        }
        
        static string LoadGame2()
        {
            LoadGame(2);
            return "";
        }
        
        static string LoadGame3()
        {
            LoadGame(3);
            return "";
        }
        
        static string SaveGame1()
        {
            NewGame(1);
            return "";
        }
        
        static string SaveGame2()
        {
            NewGame(2);
            return "";
        }
        
        static string SaveGame3()
        {
            NewGame(3);
            return "";
        }
        
        // ----------------------------------
        
        static string SaveSettings()
        { 
            Console.Clear();
            
            var boardHeight = 0; 
            var boardWidth = 0; 
            var mineCount = 0;
            var userCancel = false;


            (boardHeight, userCancel) = GetUserInput("Enter board height", 8, 22, 0);
            if (userCancel) return "";
            (boardWidth, userCancel) = GetUserInput("Enter board width", 8, 50, 0);
            if (userCancel) return "";
            (mineCount, userCancel) = GetUserInput("Enter mine count or write 1 for generated", 1, (boardHeight * boardWidth / 3), 0);
            if (userCancel) return "";

            if (mineCount == 1)
            {
                mineCount = boardHeight * boardWidth / 5;
            }

            _gameSettings.BoardHeight = boardHeight; 
            _gameSettings.BoardWidth = boardWidth; 
            _gameSettings.MineCount = mineCount;
            GameConfigHandler.SaveConfig(_gameSettings);
            
            return "";
        }

        static string NewGame(int id)
        {
            var exists = false;

            using (var ctx = new AppDbContext(dbOption))
            {
                if (ctx.FileGames.Any(i => i.FileGameId == id))
                {
                    exists = true;
                    Console.WriteLine("There seems to be a game in progress on this save. Do you want to overwrite it? (y - yes ; n - no)");
                    var userInput = Console.ReadLine()?.Trim().ToUpper() ?? ""; 
                    if (userInput == "N")
                    { 
                        return "";
                    }
                }
            }
            
            var game = new Game(_gameSettings);
                
            _gameState.Board = game.Board;
            _gameState.BoardCopy = game.BoardCopy;
            _gameState.BoardHeight = _gameSettings.BoardHeight;
            _gameState.BoardWidth = _gameSettings.BoardWidth;
            _gameState.MineCount = _gameSettings.MineCount;
            _gameState.MineLocations = game.MineList;

            var gameJson = GameStateHandler.Serialize(_gameState);

            using (var ctx = new AppDbContext(dbOption))
            {
                if (exists)
                {
                    var gameFile = ctx.FileGames.FirstOrDefault(i => i.FileGameId == id);
                    gameFile.FileGameJson = gameJson;

                    ctx.FileGames.Update(gameFile);
                    ctx.SaveChanges();
                }
                else
                {
                    ctx.FileGames.Add(new FileGame()
                    {
                        FileGameId = id,
                        FileGameName = "Game",
                        FileGameJson = gameJson
                    });
                                    
                    ctx.SaveChanges();
                }
            }

            Console.WriteLine(game.Board);
            PlayGame(game, id);
            
            return "";
        }

        static string LoadGame(int id)
        {
            using (var ctx = new AppDbContext(dbOption))
            {
                if (!ctx.FileGames.Any(i => i.FileGameId == id))
                {
                    Console.WriteLine("There is no save game. 'Press any key to continue'.");
                    Console.ReadKey();
                    return "";
                }
                var savedGame = ctx.FileGames.Single(i => i.FileGameId == id);
                var saveGameState = GameStateHandler.Deserialize(savedGame.FileGameJson);
                _gameState = saveGameState;
            }

            var game = new Game(_gameState);

            PlayGame(game, id);
            return "";
        }

        static string PlayGame(Game game, int id)
        {
            var done = false;
            //Align game
            _gameState.Board = game.Board;
            _gameState.BoardCopy = game.BoardCopy; 
            _gameState.Board = game.Board; 
            _gameState.BoardCopy = game.BoardCopy; 
            _gameState.BoardHeight = game.BoardHeight; 
            _gameState.BoardWidth = game.BoardWidth; 
            _gameState.MineCount = game.MineCount; 
            _gameState.MineLocations = game.MineList;
            
            do
            {
                Console.Clear();
                GameUI.PrintBoard(game);
                
                
                // var for game over?

                //Save
                //TODO: MOVE THIS INSIDE GAME ENGINE?
                using (var ctx = new AppDbContext(dbOption))
                {
                    var gameJson = GameStateHandler.Serialize(_gameState);
                    var gameFile = ctx.FileGames.FirstOrDefault(i => i.FileGameId == id);
                    gameFile.FileGameJson = gameJson;

                    ctx.FileGames.Update(gameFile);
                    ctx.SaveChanges();
                }

                var userYint = 0;
                var userXint = 0;
                var userFlag = false;
                var cancelGame = false;

                //Movement
                (userXint, cancelGame) = GetUserInput("Enter X coordinate", 1, game.BoardWidth, 0);
                if (!cancelGame)
                {
                    (userYint, cancelGame) = GetUserInput("Enter Y coordinate", 1, game.BoardHeight, 0);
                }
                if (!cancelGame)
                {
                    (userFlag, cancelGame) = GetUserFlagInput("Place flag?", 0);
                }
                
                if (cancelGame)
                {
                    done = true;
                }
                else
                {
                    game.Move(userYint - 1, userXint - 1, userFlag);
                }

                bool check = game.CheckConditions();

                if (check)
                {
                    done = true;
                    Console.Clear();
                    GameUI.PrintBoard(game);
                    Console.WriteLine("GAME OVER");
                    Console.ReadKey();
                }

            } while (!done);
            
            return "";
        }

        static (int result, bool wasCanceled) GetUserInput(string prompt, int min, int max, int? cancelIntValue = null,
            string cancelStrValue = "")
        {
            do
            {
                Console.WriteLine(prompt);
                Console.WriteLine($"The minimum value can be {min} and the max value can be {max}.");
                if (cancelIntValue.HasValue || !string.IsNullOrEmpty(cancelStrValue))
                {
                    Console.WriteLine($"To cancel input enter: {cancelIntValue}" +
                                      $"{(cancelIntValue.HasValue && !string.IsNullOrWhiteSpace(cancelStrValue) ? " or " : "")}" +
                                      $"{cancelStrValue}");
                }
                
                Console.Write("> ");
                var consoleLine = Console.ReadLine();

                if (consoleLine == cancelStrValue) return (0, true);
                

                if (int.TryParse(consoleLine, out var userInt))
                {
                    
                    if (userInt >= min && userInt <= max || userInt == cancelIntValue)
                    {
                        return userInt == cancelIntValue ? (userInt, true) : (userInt, false);
                    }
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine($"{userInt} isn't in the range {min} - {max}!");
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine($"'{consoleLine}' can't be converted to int value!");
                }
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine();
            } while (true);
        }
        
        static (bool placeFlag, bool wasCanceled) GetUserFlagInput(string prompt, int? cancelIntValue = null,
            string cancelStrValue = "")
        {
            do
            {
                Console.WriteLine(prompt);
                Console.WriteLine("Do you wish to place a flag? (F to place flag). Place a flag or move on flag to remove it!");
                if (cancelIntValue.HasValue || !string.IsNullOrEmpty(cancelStrValue))
                {
                    Console.WriteLine($"To cancel input enter: {cancelIntValue}" +
                                      $"{(cancelIntValue.HasValue && !string.IsNullOrWhiteSpace(cancelStrValue) ? " or " : "")}" +
                                      $"{cancelStrValue}");
                }
                
                Console.Write("> ");
                var consoleLine = Console.ReadLine();

                if (consoleLine == cancelStrValue) return (false, true);

                if (string.IsNullOrWhiteSpace(consoleLine))
                {
                    return (false, false);
                }

                if (consoleLine.ToLower().Trim() == "f")
                {
                    return (true, false);
                }
                
                return (false, false);
            } while (true);
        }
    }
}