﻿using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class FileGame
    {
        public int FileGameId { get; set; }

        [Required]
        [MaxLength(16)]
        [MinLength(3)]
        public string FileGameName { get; set; } = default!;

        [Required]
        [MinLength(10)]
        public string FileGameJson { get; set; } = default!;
    }
}