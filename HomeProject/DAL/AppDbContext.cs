﻿using System;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace DAL
{
    public class AppDbContext : DbContext
    {
        public DbSet<FileGame> FileGames { get; set; } = default!;
  
        /*
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder
                .UseSqlite(@"Data Source=E:/Kool/5.Semester/C#/Projekt/HomeProject/ConsoleApp/minesweeper.db");
        }
        */
        //TODO: SWITCH
        public AppDbContext(DbContextOptions options) : base(options)
        {
            
        }
        

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}