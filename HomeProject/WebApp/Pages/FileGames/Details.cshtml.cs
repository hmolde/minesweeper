using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using DAL;
using Domain;
using GameEngine;

namespace WebApp.Pages_FileGames
{
    public class DetailsModel : PageModel
    {
        private readonly DAL.AppDbContext _context;

        private Game _game;
        private GameState _gameState;

        //DATA TO USE ON HTML
        public SquareState[,] gameBoard;
        public int boardHeight = 8;
        public int boardWidth = 8;
        public int mineCount = 10;
        public bool gameOver;
        
        //Data to play
        [BindProperty] public bool placeFlag { get; set; } = false;
        [BindProperty] 
        [Range(1, 10)]
        public int squareX { get; set; } = 0;
        
        [BindProperty]
        [Range(1, 10)]
        public int squareY { get; set; } = 0;
        

        public DetailsModel(DAL.AppDbContext context)
        {
            _context = context;
        }

        public FileGame FileGame { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            FileGame = await _context.FileGames.FirstOrDefaultAsync(m => m.FileGameId == id);

            if (FileGame == null)
            {
                return NotFound();
            }
            
            LoadData();

            return Page();
        }

        public async Task<ActionResult> OnPost(int? id)
        {
            FileGame = await _context.FileGames.FirstOrDefaultAsync(m => m.FileGameId == id);

            if (FileGame == null)
            {
                return NotFound();
            }
            
            LoadData();
            _game.Move(squareY, squareX, placeFlag);
            gameOver = _game.CheckConditions();

            using (_context)
            {
                var gameJson = GameStateHandler.Serialize(_gameState);
                var gameFile = _context.FileGames.FirstOrDefault(i => i.FileGameId == id);
                gameFile.FileGameJson = gameJson;

                _context.FileGames.Update(gameFile);
                _context.SaveChanges();   
            }
            
            return Page();
        }

        public string ReturnSquareState(SquareState squareState)
        {
            switch (squareState)
            {
                case SquareState.NotSelected:
                    return " ";
                case SquareState.Empty:
                    return "~";
                case SquareState.Mine:
                    return "X";
                case SquareState.Flag:
                    return "F";
                case SquareState.Bomb1:
                    return "1";
                case SquareState.Bomb2:
                    return "2";
                case SquareState.Bomb3:
                    return "3";
                case SquareState.Bomb4:
                    return "4";
                case SquareState.Bomb5:
                    return "5";
                case SquareState.Bomb6:
                    return "6";
                case SquareState.Bomb7:
                    return "7";
                case SquareState.Bomb8:
                    return "8";
                default:
                    throw new InvalidEnumArgumentException("Unknown enum option!");
            }
        }

        private void LoadData()
        {
            var saveGameState = GameStateHandler.Deserialize(FileGame.FileGameJson);
            _gameState = saveGameState;
            
            _game = new Game(_gameState);
            
            gameBoard = _gameState.Board;
            mineCount = _gameState.MineCount;
            boardHeight = _gameState.BoardHeight;
            boardWidth = _gameState.BoardWidth;
            
            gameOver = _game.CheckConditions();
        }
    }
}
