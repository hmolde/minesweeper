using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using DAL;
using Domain;
using GameEngine;
using Microsoft.AspNetCore.Routing;

namespace WebApp.Pages_FileGames
{
    public class IndexModel : PageModel
    {
        private readonly DAL.AppDbContext _context;
        
        [BindProperty]
        [Range(1, 50)]
        public int height { get; set; }
        
        [BindProperty]
        [Range(1, 50)]
        public int width { get; set; }
        
        [BindProperty]
        public int mines { get; set; }
        
        [BindProperty]
        [MinLength(2)]
        [MaxLength(32)]
        [Required]
        public string name { get; set; }

        private GameSettings _GameSettings = new GameSettings();
        private GameState _gameState = new GameState();
        private Game _game;

        public IndexModel(DAL.AppDbContext context)
        {
            _context = context;
        }

        public IList<FileGame> FileGames { get;set; } = new List<FileGame>();

        public async Task OnGetAsync()
        {
            FileGames = await _context.FileGames.ToListAsync();
        }
        
        public async Task OnPostStartGame1()
        {
            FileGames = await _context.FileGames.ToListAsync();
            startGame(1);
        }
        
        public async Task OnPostStartGame2()
        {
            FileGames = await _context.FileGames.ToListAsync();
            startGame(2);
        }
        
        public async Task OnPostStartGame3()
        {
            FileGames = await _context.FileGames.ToListAsync();
            startGame(3);
        }

        private PageResult startGame(int id)
        {
            
            if (!ModelState.IsValid)
            {
                return Page();
            }
            
            ChangeSettings();
            
            _game = new Game(_GameSettings);
            
            CreateState();

            var gameJson = GameStateHandler.Serialize(_gameState);
            
            using (_context)
            {
                if (_context.FileGames.Any(i => i.FileGameId == id))
                {
                    var gameFile = _context.FileGames.FirstOrDefault(i => i.FileGameId == id);
                    gameFile.FileGameJson = gameJson;
                    gameFile.FileGameName = _GameSettings.GameName;

                    _context.FileGames.Update(gameFile);
                    _context.SaveChanges();
                }
                else
                {
                    _context.FileGames.Add(new FileGame()
                    {
                        FileGameId = id,
                        FileGameName = _GameSettings.GameName,
                        FileGameJson = gameJson
                    });
                                    
                    _context.SaveChanges();
                }
            }
            
            return Page();
        }

        private void ChangeSettings()
        {
            _GameSettings.BoardHeight = height;
            _GameSettings.BoardWidth = width;
            _GameSettings.MineCount = mines;
            _GameSettings.GameName = name;
        }

        private void CreateState()
        {
            _gameState.Board = _game.Board;
            _gameState.BoardCopy = _game.BoardCopy;
            _gameState.BoardHeight = _GameSettings.BoardHeight;
            _gameState.BoardWidth = _GameSettings.BoardWidth;
            _gameState.MineCount = _GameSettings.MineCount;
            _gameState.MineLocations = _game.MineList;
        }
    }
}
