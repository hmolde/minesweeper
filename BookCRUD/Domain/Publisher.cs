using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Publisher
    {
        public int PublisherId { get; set; }

        [Required]
        [MaxLength(32, ErrorMessage = "Max length for {0} is {1}")]
        [MinLength(3, ErrorMessage = "Min length for {0} is {1}")]
        public string PublisherName { get; set; } = default!;

        public ICollection<Book>? Books { get; set; } = default!;
    }
}