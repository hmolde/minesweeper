using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Language
    {
        public int LanguageId { get; set; }

        [Required]
        [MaxLength(16, ErrorMessage = "Max length for {0} is {1}")]
        [MinLength(1, ErrorMessage = "Min length for {0} is {1}")]
        public string LanguageName { get; set; } = default!;

        public ICollection<Book>? Books { get; set; }
    }
}