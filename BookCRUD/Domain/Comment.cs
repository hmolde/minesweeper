using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Comment
    {
        public int CommentId { get; set; }

        [Required]
        [MaxLength(256, ErrorMessage = "Max length for {0} is {1}")]
        [MinLength(4, ErrorMessage = "Min length for {0} is {1}")]
        public string CommentValue { get; set; } = default!;
        
        public int BookId { get; set; }
        public Book? Book { get; set; } = default!;
    }
}