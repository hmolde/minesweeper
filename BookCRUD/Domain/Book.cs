﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Book
    {
        public int BookId { get; set; }

        [Required]
        [MaxLength(64, ErrorMessage = "Max length for {0} is {1}")]
        [MinLength(4, ErrorMessage = "Min length for {0} is {1}")]
        public string BookTitle { get; set; } = default!;

        [Required]
        [MaxLength(256, ErrorMessage = "Max length for {0} is {1}")]
        public string? BookDescription { get; set; }

        /*
        [Required]
        [DataType(DataType.Date)]
        public DateTime BookRelease { get; set; } = default!;
        */

        public int BookPublishingYear { get; set; }
        
        public int BookWordCount { get; set; }
        
        public int PublisherId { get; set; }
        public Publisher? Publisher { get; set; } = default!;

        public int LanguageId { get; set; }
        public Language? Language { get; set; }

        public ICollection<BookAuthor>? BookAuthors { get; set; } = default!;
        public ICollection<Comment>? Comments { get; set; }
    }
}