using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.InteropServices.ComTypes;

namespace Domain
{
    public class Author
    {
        public int AuthorId { get; set; }

        [Required]
        [MaxLength(64, ErrorMessage = "Max length for {0} is {1}")]
        [MinLength(1, ErrorMessage = "Min length for {0} is {1}")]
        [Display(Name = "Given name", Prompt = "Your given first name")]
        public string AuthorFirstName { get; set; } = default!;
        
        [Required]
        [MaxLength(64, ErrorMessage = "Max length for {0} is {1}")]
        [MinLength(1, ErrorMessage = "Min length for {0} is {1}")]
        [Display(Name = "Family name", Prompt = "Your family name")]
        public string AuthorLastName { get; set; } = default!;

        [Display(Name = "Birth date", Prompt = "Your date of birth (year-month-date)")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        //[Range(1900, 2019, ErrorMessage = "Keep {0} in range of {1} to {2}")]
        public DateTime DOB { get; set; }
        
        
        public ICollection<BookAuthor>? AuthorBooks { get; set; } = default!;

        
        [Display(Name = "Full name")]
        public string FirstLastName => AuthorFirstName + " " + AuthorLastName;
        
        [Display(Name = "Full name")]
        public string LastFirstName => AuthorLastName + " " + AuthorFirstName;

    }
}