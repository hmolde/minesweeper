using Domain;

namespace WebApp.DTO
{
    public class AuthorIndexDTO
    {
        public Author Author { get; set; } = default!;
        public int AuthorBookCount { get; set; }
    }
}