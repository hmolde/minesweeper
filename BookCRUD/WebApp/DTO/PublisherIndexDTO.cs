using Domain;

namespace WebApp.DTO
{
    public class PublisherIndexDTO
    {
        public Publisher Publisher { get; set; } = default!;
        public int PublisherBookCount { get; set; }
    }
}