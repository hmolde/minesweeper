using System.Collections.Generic;
using Domain;

namespace WebApp.DTO
{
    public class BookIndexDTO
    {
        public Book Book { get; set; } = default!;
        public int BookCommentCount { get; set; }
        public string? BookLastComment { get; set; }
    }
}