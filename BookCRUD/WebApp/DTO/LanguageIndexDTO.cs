using Domain;

namespace WebApp.DTO
{
    public class LanguageIndexDTO
    {
        public Language Language { get; set; } = default!;
        public int LanguageBookCount { get; set; }
    }
}