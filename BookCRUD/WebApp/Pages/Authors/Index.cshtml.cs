using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using DAL;
using Domain;
using WebApp.DTO;

namespace WebApp.Pages_Authors
{
    public class IndexModel : PageModel
    {
        private readonly DAL.AppDbContext _context;

        public IndexModel(DAL.AppDbContext context)
        {
            _context = context;
        }

        public IList<Author> Author { get;set; }
        public IList<AuthorIndexDTO> Authors { get; set; }

        public async Task OnGetAsync()
        {
            var authorQuery = _context.Authors
                .Include(a => a.AuthorBooks)
                    .ThenInclude(a => a.Book)
                .Select(a => new AuthorIndexDTO()
                {
                    Author = a,
                    AuthorBookCount= 0
                })
                .AsQueryable();

            Authors = await authorQuery.ToListAsync();

            //THIS IS NOT GOOD AT ALL!
            foreach (var author in Authors)
            {
                author.AuthorBookCount = await _context.BookAuthors
                    .Where(c => c.AuthorId == author.Author.AuthorId).CountAsync();
            }
            
            /*
            Author = await _context.Authors
                .ToListAsync();
            */
        }
    }
}
