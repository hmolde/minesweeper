using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using DAL;
using Domain;

namespace WebApp.Pages_BookAuthors
{
    public class CreateModel : PageModel
    {
        private readonly DAL.AppDbContext _context;

        public CreateModel(DAL.AppDbContext context)
        {
            _context = context;
        }

        public SelectList BookAuthorAuthorSelectList { get; set; }
        public SelectList BookAuthorBookSelectList { get; set; }
        
        public IActionResult OnGet()
        {
            BookAuthorAuthorSelectList = 
                new SelectList(_context.Authors, nameof(Author.AuthorId), nameof(Author.AuthorFirstName));
            BookAuthorBookSelectList = 
                new SelectList(_context.Books, nameof(Book.BookId), nameof(Book.BookTitle));
            return Page();
        }

        [BindProperty] public BookAuthor BookAuthor { get; set; } = default!;

        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                BookAuthorAuthorSelectList = 
                    new SelectList(_context.Authors, nameof(Author.AuthorId), nameof(Author.AuthorFirstName));
                BookAuthorBookSelectList = 
                    new SelectList(_context.Books, nameof(Book.BookId), nameof(Book.BookTitle));
                return Page();
            }

            _context.BookAuthors.Add(BookAuthor);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
