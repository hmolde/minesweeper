using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using DAL;
using Domain;
using WebApp.DTO;

namespace WebApp.Pages_Books
{
    public class IndexModel : PageModel
    {
        private readonly DAL.AppDbContext _context;

        public IndexModel(DAL.AppDbContext context)
        {
            _context = context;
        }

        public IList<Book> Book { get;set; }
        public IList<BookIndexDTO> Books { get;set; }

        public string? Search { get; set; }

        //TODO: Think about moving business logic later
        public async Task OnGetAsync(string? search, string? ResetSearch)
        {
            if (ResetSearch == "Clear")
            {
                search = "";
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(search))
                {
                    Search = search.ToLower().Trim();
                }
            }

            var bookQuery = _context.Books
                .Include(b => b.Language)
                .Include(b => b.Publisher)
                .Include(b => b.BookAuthors)
                    .ThenInclude(a => a.Author)
                //.Include(b => b.Comments)
                .Select(a => new BookIndexDTO()
                {
                    Book = a, 
                    BookCommentCount = a.Comments.Count, 
                    BookLastComment = ""//a.Comments.LastOrDefault().CommentValue
                })
                .AsQueryable();

            if (!string.IsNullOrWhiteSpace(Search))
            {
                bookQuery = bookQuery
                    .Where(b =>
                        b.Book.BookTitle.ToLower().Contains(Search) ||
                        b.Book.BookDescription.ToLower().Contains(Search) ||
                        b.Book.Language.LanguageName.ToLower().Contains(Search) ||
                        b.Book.Publisher.PublisherName.ToLower().Contains(Search) ||
                        b.Book.BookAuthors.Any(a =>
                            a.Author.AuthorFirstName.ToLower().Contains(Search) ||
                            a.Author.AuthorLastName.ToLower().Contains(Search))
                        );
            }
            
            bookQuery = bookQuery.OrderBy(b => b.Book.BookTitle);
            Books = await bookQuery.ToListAsync();

            //THIS IS NOT GOOD AT ALL!
            foreach (var book in Books)
            {
                //book.BookCommentCount = await _context.Comments.Where(c => c.BookId == book.Book.BookId).CountAsync();
                book.BookLastComment = (
                    await _context.Comments
                        .Where(c => c.BookId == book.Book.BookId)
                        .OrderByDescending(c => c.CommentId).FirstOrDefaultAsync())?.CommentValue;
            }
        }
    }
}
