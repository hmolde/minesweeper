using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using DAL;
using Domain;
using WebApp.DTO;

namespace WebApp.Pages_Publishers
{
    public class IndexModel : PageModel
    {
        private readonly DAL.AppDbContext _context;

        public IndexModel(DAL.AppDbContext context)
        {
            _context = context;
        }

        public IList<Publisher> Publisher { get;set; }
        public IList<PublisherIndexDTO> Publishers { get; set; }

        public async Task OnGetAsync()
        {
            var publisherQuery = _context.Publishers
                .Select(a => new PublisherIndexDTO()
                {
                    Publisher = a,
                    PublisherBookCount = 0
                })
                .AsQueryable();

            Publishers = await publisherQuery.ToListAsync();

            //THIS IS NOT GOOD AT ALL!
            foreach (var publisher in Publishers)
            {
                publisher.PublisherBookCount = await _context.Books
                    .Where(c => c.PublisherId == publisher.Publisher.PublisherId).CountAsync();
            }

            /*
            Publisher = await _context.Publishers
                .ToListAsync();
            */
        }
    }
}
