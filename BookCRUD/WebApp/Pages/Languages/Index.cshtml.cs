using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using DAL;
using Domain;
using WebApp.DTO;

namespace WebApp.Pages_Languages
{
    public class IndexModel : PageModel
    {
        private readonly DAL.AppDbContext _context;

        public IndexModel(DAL.AppDbContext context)
        {
            _context = context;
        }

        public IList<Language> Language { get;set; }
        public IList<LanguageIndexDTO> Languages { get; set; }

        public async Task OnGetAsync()
        {
            var languageQuery = _context.Languages
                .Select(a => new LanguageIndexDTO()
                {
                    Language = a,
                    LanguageBookCount= 0
                })
                .AsQueryable();

            Languages = await languageQuery.ToListAsync();

            //THIS IS NOT GOOD AT ALL!
            foreach (var language in Languages)
            {
                language.LanguageBookCount = await _context.Books
                    .Where(c => c.LanguageId == language.Language.LanguageId).CountAsync();
            }
        
            /*
            Language = await _context.Languages
                .ToListAsync();
            */
        }
    }
}
